import React,{Component} from 'react';
import { StyleSheet,View,Text} from 'react-native';
import AppLogin from './src/auth/AppLogin.js';
import AppSignup from './src/auth/AppSignup.js';
import Dashboard from './src/app/Dashboard.js';


export default class App extends Component {
  render() {
    return (

      <View style={styles.fullBody}>
        <View style={styles.header} >
          <Text style={styles.textAlign}> First APP </Text>
        </View>
        
        <View style={styles.container} ><Dashboard/></View>
        <View style={styles.footer}>
          <Text style={styles.footerText}>Contact us</Text>
        </View>
      </View>
      
      
    );
  }
};

const styles = StyleSheet.create({
  
  fullBody:{ 
    flex            : 1,
    flexDirection   : 'column',
    justifyContent  : 'space-between',
    alignItems      : 'stretch',
  },
  header:{
    height          : 100,
    backgroundColor : 'red'
  },
  container: {
    height          : 180,
    padding : 0
  },
  footer:{
    height          : 50, 
    backgroundColor: 'black'
  },
  textAlign: {

    textAlign      : "center",
    justifyContent : 'center',
    fontSize       : 20,
    padding        : 30,
    fontWeight     : 'bold',
    color          : '#ffffff'
   
  },
  footerText:{
    textAlign      : "center",
    justifyContent : 'center',
    fontSize       : 20,
    padding        : 20,
    fontWeight     : 'bold',
    color          : '#ffffff'
  }


});
/*export default function App() {
  return (
         
           <View style={styles.header}/>
           <AppLogin/>


         

   
 
    );
}

const styles = StyleSheet.create({
  header:{
    flex:1,
    width :100,
    height: 50,

  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textAlign: {
    textAlign: "center"
  }
});
 */