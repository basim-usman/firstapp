import React from 'react';
import { StyleSheet, Text, View ,Button} from 'react-native';
import AppButton from '../components/AppButton.js';
import AppTextInput from '../components/AppTextInput.js';


export default function AppSignup() {
    return(

        <View style={styles.container}>
                
        <AppTextInput/>
        <AppTextInput/>
       

     
        <AppButton>SignUp </AppButton>
        
    </View>


    );
}



const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 23
    },
    textInputStyle:{
        height:'30%', 
        width: '50%',
        borderColor: '#0000FF',
        borderWidth: 2,
        marginBottom:10,
        textAlign:'center'
    },

  });