import React from 'react';
import { StyleSheet,View,Text,TextInput} from 'react-native'
import AppButton from '../components/AppButton.js';
import AppTextInput from '../components/AppTextInput.js';



export default class AppLogin extends React.Component{
    render(){

        return(
            
            <View style={styles.container}>
                
                <AppTextInput/>

             
                <AppButton>Sign In </AppButton>
                
            </View>
    
    
        );

    }

}



const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 23
    },
 

  });