import React from 'react';
import {Text,StyleSheet,TouchableOpacity} from 'react-native';


const AppButton = (props)=>{

    return(
        <TouchableOpacity style={styles.buttonBody} 
                          onPress={props.onPress}>
            
            <Text style={styles.buttonText}>
                {props.children}
            </Text>

        </TouchableOpacity>
    );
}

const styles= StyleSheet.create({
    buttonBody:{
        backgroundColor: '#00aeef',
        width: '50%',
        padding: 20,
        alignItems: 'center',
        height:'30%',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 14,
        fontWeight: '600',
    }
})

export default AppButton;