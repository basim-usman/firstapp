import React from 'react';
import { StyleSheet,Image } from 'react-native'

const AppImage = () => (
   <Image source = {require('../../assets/profile.jpeg')} style={styles.imageStyle}/>
)

const styles = StyleSheet.create({
    imageStyle:{
        width: 50, 
        height: 50,
        marginBottom:10
    }
});


export default AppImage