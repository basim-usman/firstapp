import React from 'react';
import {StyleSheet,Text} from 'react-native';

const AppText =(props)=>{

    return(
        <Text style={labelStyle.textStyle}>{props.children}</Text>
    );

}

const labelStyle = StyleSheet.create({
    textStyle :{
        justifyContent:'center',
        fontSize:10,
        fontWeight:"bold",
        color:"#ffffff"
    }
});