import React,{Component} from 'react';
import { View, TextInput, StyleSheet } from 'react-native'

class AppTextInput extends Component {
   state = {
      username: '',
      password: ''
   }
   handleUsername = (text) => {
      this.setState({ username: text })
   }
   handlePassword = (text) => {
      this.setState({ password: text })
   }

   render() {
      return (
         <View style = {styles.container}>
            <TextInput style = {styles.textInputStyle}
               underlineColorAndroid = "transparent"
               placeholder="Username"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.handleUsername}/>
            
            <TextInput style = {styles.textInputStyle}
               underlineColorAndroid = "transparent"
               placeholder = "Password"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               onChangeText = {this.handlePassword}/>
            
         </View>
      )
   }

}

const styles = StyleSheet.create({
    container: {
       
       width :"100%",
       justifyContent:'center',
       alignItems: 'center',
    },
  
     textInputStyle:{
         height: 40,
         width: '50%',
         borderColor: '#0000FF',
         borderWidth: 2,
         marginBottom:10,
         textAlign:'center'
     },
 })
export default AppTextInput;

  